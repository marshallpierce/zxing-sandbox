package org.mpierce.zxing;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CoversheetTest {

    @Test
    public void testReadCoversheet() throws IOException, NotFoundException {
        doCoversheetTest("/coversheet.png");
    }

    @Test
    public void testReadCoversheetCrop() throws IOException, NotFoundException {
        doCoversheetTest("/datamatrix-crop.png");
    }

    @Test
    public void testReadCoversheetSquareCrop() throws IOException, NotFoundException {
        doCoversheetTest("/coversheet-square-crop.png");
    }

    @Test
    public void testReadCoversheetWithScan() throws IOException {
        BufferedImage image = ImageIO.read(getClass().getResourceAsStream("/coversheet.png"));

        // create a square that we can scoot down the image
        int left = image.getWidth() / 4;
        // data matrix is always in the middle half
        int width = image.getWidth() / 2;
        int height = width;
        Result result = null;

        for (int top = 0; top < image.getHeight() - width; top++) {
            System.out.println("Trying top " + top);
            BufferedImageLuminanceSource luminanceSource =
                new BufferedImageLuminanceSource(image, left, top, width, height);
            BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(luminanceSource));

            Map<DecodeHintType, Object> decodeHints = newHashMap();
            decodeHints.put(DecodeHintType.CHARACTER_SET, ISO_8859_1.name());
            decodeHints.put(DecodeHintType.POSSIBLE_FORMATS, newArrayList(BarcodeFormat.DATA_MATRIX));
            decodeHints.put(DecodeHintType.TRY_HARDER, true);

            try {

                result = new MultiFormatReader().decode(binaryBitmap, decodeHints);
                break;
            } catch (NotFoundException e) {
                continue;
            }
        }

        if (result == null) {
            fail("Could not find a barcode");
        }

        assertEquals("{\"_v\":1,\"sdmsDocToken\":\"/RufUoE8n2gdMOYe8lEHsQ==\"} ", result.getText());
    }

    private void doCoversheetTest(String imagePath) throws IOException, NotFoundException {
        BufferedImage image = ImageIO.read(getClass().getResourceAsStream(imagePath));

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));

        Map<DecodeHintType, Object> decodeHints = newHashMap();
        decodeHints.put(DecodeHintType.CHARACTER_SET, ISO_8859_1.name());
        decodeHints.put(DecodeHintType.POSSIBLE_FORMATS, newArrayList(BarcodeFormat.DATA_MATRIX));
        decodeHints.put(DecodeHintType.TRY_HARDER, true);

        Result result = new MultiFormatReader().decode(binaryBitmap, decodeHints);

        assertEquals("{\"_v\":1,\"sdmsDocToken\":\"/RufUoE8n2gdMOYe8lEHsQ==\"} ", result.getText());
    }
}
