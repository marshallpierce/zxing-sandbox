package org.mpierce.zxing;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;

public class RoundtripTest {

    @Test
    public void testQR() throws WriterException, FormatException, ChecksumException, NotFoundException, IOException {
        doTest(BarcodeFormat.QR_CODE);
    }

    @Test
    public void testDataMatrix() throws WriterException, FormatException, ChecksumException, NotFoundException,
        IOException {
        doTest(BarcodeFormat.DATA_MATRIX);
    }

    static void doTest(BarcodeFormat format) throws IOException, FormatException, ChecksumException, WriterException,
        NotFoundException {
        String filename = "./zxing.png";
        String data = "Sample zebra crossing data";
        String charset = "ISO-8859-1";

        Map<EncodeHintType, Object> hints = newHashMap();
        hints.put(EncodeHintType.CHARACTER_SET, charset);
        hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);

        Map<DecodeHintType, Object> decodeHints = newHashMap();
        decodeHints.put(DecodeHintType.CHARACTER_SET, charset);
        decodeHints.put(DecodeHintType.POSSIBLE_FORMATS, newArrayList(format));
        decodeHints.put(DecodeHintType.TRY_HARDER, true);
        decodeHints.put(DecodeHintType.PURE_BARCODE, true);

        try {
            write(filename, data, hints, 300, 300, format);

            assertEquals(data, (read(filename, decodeHints)));
        } finally {
            Files.delete(Paths.get(filename));
        }
    }

    static void write(String filename, String data, Map<EncodeHintType, ?> hints, int width,
        int height, BarcodeFormat format) throws
        IOException, WriterException {
        BitMatrix matrix = new MultiFormatWriter().encode(data, format, width, height, hints);

        MatrixToImageWriter.writeToFile(matrix, filename.substring(filename.lastIndexOf('.') + 1), new File(filename));
    }

    static String read(String filename, Map<DecodeHintType, ?> hints) throws IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(
            new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filename)))));
        Result result = new MultiFormatReader().decode(binaryBitmap, hints);

        return result.getText();
    }
}
